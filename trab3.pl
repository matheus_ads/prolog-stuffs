/*
Trabalho 1 C - INE5416 - Prolog
Matheus Amorim de Souza - 12200651
Cadeia de disciplinas de Fonoaudiologia
Obs: Testado no SWI-PROLOG Version 7.3.25-3-gc3a87c2 para OS X - El Capitan e
SWIPL 7.2.3 no Ubuntu 16.04
*/

/* Regra para insercao de disciplinas
   CODIGO_CAGR (codigo igual ao do cagr em letras minusculas
   FASE (int)
   NOME (string)
*/


/* Disciplinas da primeira fase */

disciplina(fon7101, 1, "Caracterizacao do Ser Humano Saudavel I").
disciplina(fon7102, 1, "Introducao ao Estudo da Fonoaudiologia I").
disciplina(fon7103, 1, "Contexto Social e Saude Coletiva").
disciplina(fon7104, 1, "Habilidades Auditiva, Vocal e Articulatoria").
disciplina(fon7203, 1, "Metodos e Tecnica de Pesquisa I").
disciplina(lsb7904, 1, "Lingua Brasileira de Sinais").


/* Disciplinas da segunda fase */

disciplina(fon7201, 2, "Ser Humano Saudavel I").
disciplina(fon7202, 2, "Contexto Social e Saude Coletiva").
disciplina(fon7205, 2, "Caracterizacao do Ser Humano Saudavel II").
disciplina(fon7206, 2, "Introducao ao Estudo da Fonoaudiologia II").
disciplina(nfr7004, 2, "Biosseguranca").
disciplina(fon7604, 2, "Seminarios Interdisciplinares").

/* Disciplinas da terceira fase */

disciplina(fon7301, 3, "Ser Humano e as Alteracoes em Fonoaudiologia I").
disciplina(fon7302, 3, "Contexto Social e Saude Coletiva III").
disciplina(fon7304, 3, "Ser Humano Saudavel II").
disciplina(fon7305, 3, "Investigacao em Audiologia").
disciplina(fon7306, 3, "Etica em Saude").

/* Disciplinas da quarta fase */

disciplina(fon7303, 4, "Metodos e Tecnicas de Pesquisa II").
disciplina(fon7401, 4, "Ser Humano e as Alteracoes em Fonoaudiologia II").
disciplina(fon7402, 4, "O Processo de Investigacao Aplic. a Fono I").
disciplina(fon7403, 4, "Contexto Social e Saude Coletiva IV").

/* Disciplinas da quinta fase */

disciplina(fon7404, 5, "Metodos e Tecnicas de Pesquisa III").
disciplina(fon7501, 5, "O Processo de Investigacao Aplic a Fono II").
disciplina(fon7502, 5, "O Processo Terapeutico").
disciplina(fon7606, 5, "Atuacao Fonoaudiologica na Comunidade").

/* alunos matriculados em disciplinas */
aluno(pedro).
aluno(lucas).

matricula(pedro, fon7101).
matricula(pedro, fon7102).
matricula(pedro, fon7103).
matricula(pedro, fon7104).

matricula(lucas, fon7201).
matricula(lucas, fon7202).
matricula(lucas, fon7205).
matricula(lucas, fon7104).

/* Regras para pre requisitos */


/* Pre requisitos para disciplinas da segunda fase */
/* Regra: Disciplina(2a fase) -> Pre Requisito */

prereq(fon7201, fon7101).
prereq(fon7201, fon7102).
prereq(fon7205, fon7101).
prereq(fon7206, fon7102).

/* Pre requisitos para terceira fase */

prereq(fon7301, fon7201).
prereq(fon7301, fon7205).
prereq(fon7302, fon7202).
prereq(fon7304, fon7205).
prereq(fon7305, fon7201).
prereq(fon7305, fon7205).
prereq(fon7305, fon7206).
prereq(fon7306, fon7102).

/* Pre requisitos para quarta fase */

prereq(fon7303, fon7202).
prereq(fon7401, fon7304).
prereq(fon7402, fon7301).
prereq(fon7402, fon7305).
prereq(fon7403, fon7302).
prereq(fon7403, fon7305).

/* Pre requisitos para quinta fase */

prereq(fon7404, fon7303).
prereq(fon7501, fon7401).
prereq(fon7502, fon7402).
prereq(fon7606, fon7306).
prereq(fon7606, fon7401).
prereq(fon7606, fon7402).
prereq(fon7606, fon7403).

/* Regra para saber a fase de dada disciplina e vice versa. E a mesma regra
   com o nome da materia.
   D = disciplina(codigo), F=fase(int) N = nome(string) */

fase(D, F) :- disciplina(D, F, _).
fase_nome(D, F, N) :- disciplina(D, F, N).

/* Regra para o pre requisito da disciplina D=disciplina P=prereq da mesma */
prerequisito(D, P) :- prereq(D, P).

/* Regra item 4 do trabalho 1 A. Qual disciplina o aluno A está matriculado? */

matriculado(A, D) :- matricula(A, D).



/* Inicio Trabalho 1 B */

/* Regra 1 */
nomecompleto(D, N) :- disciplina(D, _, N).

/* Regra 2 */
precomum(D1, D2, PR) :- prereq(D1, PR), prereq(D2, PR), PR=PR.

/* Regra 3 */
prepre(D, PR) :- prereq(D, PA), prereq(PA, PB), PR = PB.

/* Regra 4 */
saopre(F, PR) :- disciplina(PR, F, _), prereq(_, PR).

/* Regra 5 */
tempre(F, D) :- disciplina(D, F, _), prereq(D, _).

/* Regra 6 */
temprecomumsaopre(F, D1, D2) :- disciplina(D1, F, _), disciplina(D2, F, _), prereq(D1, X), prereq(D2, Y), X=Y, prereq(_, D1), prereq(_, D2).

/* Regra 7 - Disciplinas da fase F que nao tem pre requisitos */
proposta(F, D) :- disciplina(D, F, _), \+ prereq(D, _).


/* Inicio Trabalho 1 C */

fase_N(F, R) :- findall(C, disciplina(C, F, _), R).

/* Regra 1 */
nfase(F, N) :- fase_N(F, L),
               length(L, N).

/* Regra 2 */
ncurso(N) :- findall(C, disciplina(C, _, _), L),
             length(L, N).

/* Regra 3 */
ntempre(N) :- findall(D, prereq(D, _), L),
              list_to_set(L, B),
              length(B, N).

/* Regra 4 */
nsaopre(N) :- findall(D, prereq(_, D), L),
              list_to_set(L, B),
              length(B, N).

/* Regra 5 */
/* npre(D, N) :- findall(D, prereq(D, _), L), length(L, N).
% apenas contando o prereq imediatamente anterior, porem nao ajuda a regra 6 */

npre(D, N) :- setof(X, disc_recurs(D, X), Qnt),
              length(Qnt, N). %recursionando pela cadeia de prereqs

/* Regra 6 */
maispre(D) :- findall(Z, npre(_, Z), Qnt),
              max(Qnt, K), npre(D, K).

/* Regra 7 */
npos(D, N) :- findall(D, prereq(_, D), L),
              length(L, N).

/* Regra 8 Nao funciona como deveria */
maispos(D) :- findall(Z, prereq(_, Z), All),
              max(All, K),
              npre(D, K).

/* Regra 9 */
maiorcadeia(L) :- findall(Z, (npre(_, Z)), All),
                  max(All, M),
                  npre(L, M).

/* Regra 10 Disciplinas com menor numero de prereqs em cadeia */
extremo(D) :- findall(Z, npre(_, Z), Qnt),
              min_list(Qnt, K), npre(D, K).

/* Regras auxiliares */
max([R], R).
max([X|Xs], R):-
    max(Xs, T),
    (X > T -> R = X ; R = T).

disc_recurs(D, N) :- prereq(D, N).
disc_recurs(D, N) :- prereq(D, X),
                     disc_recurs(X, N).
